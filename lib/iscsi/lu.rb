module ISCSI
  class LU < Entity
    def views
      context.views(name)
    end
    
    def data_file
      @node[:data_file].value
    end
  end
end