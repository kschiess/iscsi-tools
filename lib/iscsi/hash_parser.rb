module ISCSI
  class Node
    attr_reader :key, :value
    attr_accessor :parent
    attr_reader :children
    attr_reader :indent
    
    def initialize(indent, key=nil, value=nil)
      @indent = indent
      @key = key
      @value = value
      @children = []
    end
    
    def add_child child
      @children << child
      child.parent = self
    end
    
    def pretty_print q
      q.object_address_group(self) do
        if key
          q.text ' '
          q.text key.inspect
          q.text ' => '
          q.text value.inspect
        end
        
        unless children.empty?
          q.text ' childs: '
          q.group 1, '[', ']' do
            children.each do |child| 
              q.breakable
              child.pretty_print q
            end
          end
        end
      end
    end

    def [] k
      children.find { |c| c.key == k }
    end
    def size
      children.size
    end
  end
  
  class HashParser
    def parse(str)
      # Every time we indent once, we'll push a new result array onto this 
      # stack. Every time we pop the stack, we'll assign the array to the 
      # last ParsedEntity.
      root = Node.new(-1)
      
      insertion_point = root
      last = nil
      
      str.lines do |line| 
        line.chomp!
        
        if md=line.match(/^(?<indent>\s*)(?<key>(\w|\s)+)\s*: (?<val>.*)$/)
          indent = md[:indent].size
          
          # One level up: 
          if last && indent < last.indent
            insertion_point = insertion_point.parent \
              while insertion_point.indent >= indent

            fail "Assertion failed: Popped beyond root node." unless insertion_point
          end
          
          # One level deeper: 
          if last && indent > last.indent
            insertion_point = last
          end
          
          current = Node.new(indent, transform_key(md[:key]), md[:val])
          insertion_point.add_child current
          
          last = current
        end
      end

      return root.children
    end
    
    def transform_key(str)
      str.
        strip.
        gsub(' ', '_').
        downcase.
        to_sym
    end
  end
end
