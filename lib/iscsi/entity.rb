module ISCSI
  class Entity
    attr_reader :context
    
    def initialize context, node
      @context = context
      @node    = node
    end
    
    def name
      @node.value
    end
  end
end