module ISCSI
  class TargetGroup < Entity
    def targets
      @node.children.
        map { |child| context.target(child.value) }
    end
  end
end