module ISCSI
  class View < Entity
    def initialize(context, node, lu_name)
      super context, node
      @lu_name = lu_name
    end
    
    def target_group
      context.target_group(@node[:target_group].value)
    end
    
    def lu
      context.lu(@lu_name)
    end
    
    def lun
      Integer(
        @node[:lun].value)
    end
  end
end