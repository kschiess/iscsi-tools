require 'runner'

module ISCSI
  class Context
    class Category
      def initialize
        @map = {}
      end
      def access id, &producer
        @map[id] || @map[id]=producer.call
      end
    end
    
    attr_reader :runner
    
    def initialize verbose=false
      @categories = Hash.new { |h,k| h[k] = Category.new }
      @runner = ::Runner.new(verbose)
      @stmf_view_cache = {}
    end
    def cat category
      @categories[category]
    end
    
    def self.define_category singular, plural, klass, command
      cached_command = "stmf_#{plural}"

      define_method singular do |name|
        self.send(plural).
          find { |e| e.name == name }
      end
      define_method plural do
        ArrayEnum.new(self.send(cached_command)) { |node| 
          cat(singular.to_sym).access(node.value) { 
            klass.new(self, node) } }
      end
      define_method cached_command do
        variable_name = "@#{cached_command}"
        instance_variable_get(variable_name) || 
          instance_variable_set(variable_name, begin
            parser = HashParser.new
            parser.parse runner.shell(command)
          end )
      end
    end
    
    define_category :target, :targets, Target, 'stmfadm list-target -v'
    define_category :target_group, :target_groups, TargetGroup, 'stmfadm list-tg -v'
    define_category :lu, :lus, LU, 'stmfadm list-lu -v'

    def views lu
      ArrayEnum.new(stmf_views(lu)) { |node| 
        View.new(self, node, lu) }
    end
    def stmf_views lu
      @stmf_view_cache[lu] ||= begin
        parser = HashParser.new
        command = "stmfadm list-view -l #{lu}"
        parser.parse runner.shell(command)
      end
    end
  end
end