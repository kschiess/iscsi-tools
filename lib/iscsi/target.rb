module ISCSI
  class Target < Entity
    # Returns all target groups this target is in. 
    def groups
      context.target_groups.
        select { |e| e.targets.include?(self) }
    end
    
    # Returns all views of this target sorted by LUN. 
    #
    def views
      views = []
      my_groups = self.groups
       
      # Looping over all LU, then looping over all views. Store those
      # views in 'views' that concern a target group of ours.
      #
      context.lus.
        each { |lu| 
          lu.views.each { |view| 
            if my_groups.include?(view.target_group) 
              views << view
            end } }
            
      # Return sorted by LUN
      views.sort_by { |view| view.lun }
    end
  end
end