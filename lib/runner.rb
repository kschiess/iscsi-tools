require 'mixlib/shellout'

class Runner
  class CommandFailed < StandardError
    def initialize(msg, command)
      super msg
      @command = command
    end
    
    attr_reader :command
    
    def stderr
      command.stderr
    end
    def stdout
      command.stdout
    end
  end
  
  attr_reader :verbose
  def initialize verbose
    @verbose = verbose
  end
  
  def shell cmd, opts={}
    puts cmd if verbose
    
    cmd = cmd.strip
    
    command = shell_out(cmd, opts)
    command.run_command

    # Might raise 
    command.error!

    return command.stdout
  rescue Mixlib::ShellOut::ShellCommandFailed
    raise CommandFailed.new("Failed: #{cmd}\n#{command.stderr}", command)
  end
  
  def shell_out(*args)
    Mixlib::ShellOut.new(*args)
  end
end
