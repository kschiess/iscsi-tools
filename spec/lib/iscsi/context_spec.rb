require 'spec_helper'

require 'iscsi'

class FakeRunner
  def initialize(map={})
    @map = map
  end
  
  def shell cmd
    @map.each do |re, out| 
      if re.match(cmd)
        return out
      end
    end
    
    fail "No such fixture (#{cmd.inspect})"
  end
end

describe ISCSI::Context do
  slet(:ctx) { described_class.new }
  
  let(:fake_runner) { FakeRunner.new(
    /^stmfadm list-target/ => fixture('target_list.txt'), 
    /^stmfadm list-tg/     => fixture('target_groups.txt'), 
    /^stmfadm list-lu/     => fixture('lus.txt'), 
    /^stmfadm list-view -l 600144F085698100000050CB43400009/ => fixture('lu_view0.txt'), 
    /^stmfadm list-view -l 600144F085698100000050CB47E2000A/ => fixture('lu_view1.txt')) }
  before(:each) { 
    flexmock(ctx, runner: fake_runner) }
  
  describe '#targets' do
    slet(:targets) { ctx.targets }
    
    describe 'targets[0]' do
      slet(:target) { targets[0] }
      
      its(:name) { should == 'foo' }
      it "should have the correct target groups" do
        target.groups.map(&:name).should == %w(tg2 tg3)
      end 
      it "should have the correct views" do
        target.views.map(&:lu).map(&:name).should == %w()
      end 
    end
  end
  describe '#lus' do
    slet(:lus) { ctx.lus }

    describe 'lus[0]' do
      slet(:lu) { lus[0] }
      
      it "has a name" do
        lu.name.should == '600144F085698100000050CB43400009'
      end 
    end
  end
  describe '#views(LU)[0]' do
    slet(:view) { ctx.views('600144F085698100000050CB43400009')[0] }
    
    it "should link to a LU" do
      view.lu.name.should == '600144F085698100000050CB43400009'
    end 
    it "should have a target group" do
      view.target_group.name.should == "vol1"
    end 
    it "should have a LUN" do
      view.lun.should == 0
    end 
  end
end