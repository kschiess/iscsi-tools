require 'spec_helper'

require 'pp'
require 'iscsi'

describe ISCSI::HashParser do
  let(:input_string) {
'Target: iqn.2010-08.org.illumos:02:6140505e-fb64-6670-b44d-c3395b9bb201
    Operational Status: Online
    Provider Name     : iscsit
    Alias             : -
    Protocol          : iSCSI
    Sessions          : 0
        Initiator: iqn.1986-03.com.sun:01:40f581320dff.506ef076
            Alias: machine1
            Logged in since: Wed Dec 12 18:59:12 2012
Target: iqn.2010-08.org.illumos:02:6140505e-fb64-6670-b412321321312121
    Operational Status: Online
    Provider Name     : iscsit
    Alias             : -
    Protocol          : iSCSI
    Sessions          : 1
        Initiator: iqn.1986-03.com.sun:01:40f581320dff.506ef076
            Alias: machine1
            Logged in since: Wed Dec 12 18:59:12 2012'
  }

  let(:parser) { described_class.new() }
  
  describe '#parse result' do
    slet(:result) { parser.parse(input_string) }
    
    it "contains two toplevel targets" do
      result.size.should == 2
      result[0].value.should == 'iqn.2010-08.org.illumos:02:6140505e-fb64-6670-b44d-c3395b9bb201'
      result[1].value.should == 'iqn.2010-08.org.illumos:02:6140505e-fb64-6670-b412321321312121'
    end
    describe 'result[0]' do
      slet(:first) { result.first }
      
      it "contains a :provider_name" do
        first[:provider_name].value.should == 'iscsit' 
      end 
    end 
    describe 'result[1]' do
      slet(:second) { result[1] }
      
      it "should have one session stored" do
        second[:sessions].size.should == 1
        s = second[:sessions].children.first
        
        s.value.should == 'iqn.1986-03.com.sun:01:40f581320dff.506ef076'
        s[:alias].value.should == 'machine1'
      end 
    end
  end
end