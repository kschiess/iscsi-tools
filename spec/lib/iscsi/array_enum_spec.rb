require 'spec_helper'

require 'iscsi'

describe ISCSI::ArrayEnum do
  def ae ary, &block
    ISCSI::ArrayEnum.new(ary, &block)
  end
  
  describe '#to_a' do
    it "turns the enumeration into an array" do
      a = ae(%w(a b c)) { |e| e.succ }
      a.to_a.should == %w(b c d)
    end 
  end
end