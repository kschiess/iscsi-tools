def slet(name, &block)
  let name, &block
  subject { self.send(name) }
end

def fixture *args
  File.read fixture_path(*args)
end
def fixture_path *args
  File.expand_path(File.join(File.dirname(__FILE__) + "/fixture", *args))
end

RSpec.configure do |config| 
  config.mock_with :flexmock
end